<?php
require_once "config/connection.php";
session_start();

if (isset($_POST['submit'])) {
  $username = $_POST['username'];
  $password = $_POST['password'];
  $email = $_POST['email'];

  $sql = "INSERT INTO users(username, password, email) VALUES ('$username', '$password', '$email')";
  $result = $conn->query($sql);

  if ($result) {
    // $row = $result->fetch_assoc();

    // $_SESSION['username'] = $row['username'];
    header('location:index.php');
  } else {
    header('create.php');
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />
  <link rel="stylesheet" href="css/create.css" />
  <title>Create Account</title>
</head>

<body>
  <div class="container">
    <!-- <div id="close-login-btn" class="fas fa-times"></div> -->
    <div class="title">Registration</div>
    <form method="post">
      <div class="user-details">
        <div class="input-box">
          <span class="details">Username</span>
          <input type="text" placeholder="Enter your username" name="username" required />
        </div>
        <div class="input-box">
          <span class="details">Password</span>
          <input type="password" placeholder="Enter your password" name="password" required />
        </div>
        <div class="input-box">
          <span class="details">Email</span>
          <input type="email" placeholder="Enter your email" name="email" required />
        </div>
      </div>
      <div class="btn">
        <input type="submit" name="submit" value="Registration" />
      </div>
    </form>
  </div>
</body>
<script src="js/script.js"></script>

</html>