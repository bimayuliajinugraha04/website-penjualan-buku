<?php
require_once "config/connection.php";
session_start();

if (isset($_POST['submit'])) {
  $user = $_POST['username'];
  $pass = $_POST['password'];

  $sql = "SELECT * FROM users WHERE username='$user' AND password='$pass'";
  $result = $conn->query($sql);

  if ($result->num_rows > 0) {
    $row = $result->fetch_assoc();

    $_SESSION['username'] = $row['username'];
    header('location:index.php');
  } else {
    echo "<script>alert('email atau password salah')</script>";
  }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>MOCO</title>

  <link rel="stylesheet" href="https://unpkg.com/swiper@7/swiper-bundle.min.css" />

  <!-- font awesome cdn link  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" />

  <!-- custom css file link  -->
  <link rel="stylesheet" href="css/style.css" />
</head>

<body>
  <!-- header section starts  -->

  <header class="header">
    <div class="header-1">
      <a href="#" class="logo"> <i class="fas fa-book"></i> Moco </a>

      <form action="" class="search-form">
        <input type="search" name="" placeholder="search here..." id="search-box" />
        <label for="search-box" class="fas fa-search"></label>
      </form>

      <div class="icons">
        <div id="search-btn" class="fas fa-search"></div>
        <a href="#" class="fas fa-heart"></a>
        <a href="#" class="fas fa-shopping-cart"></a>
        <div id="login-btn" class="fas fa-user"></div>
      </div>
    </div>

    <div class="header-2">
      <nav class="navbar">
        <a href="#home">home</a>
        <a href="#featured">featured</a>
        <?php if (isset($_SESSION['username'])) { ?>
          <a href="logout.php">logout</a>
        <?php } else { ?>
          <a href="login.php">login</a>
        <?php } ?>
      </nav>
    </div>
  </header>

  <!-- header section ends -->

  <!-- bottom navbar  -->

  <nav class="bottom-navbar">
    <a href="#home" class="fas fa-home"></a>
    <a href="#featured" class="fas fa-list"></a>
  </nav>

  <!-- login form  -->

  <div class="login-form-container">
    <div id="close-login-btn" class="fas fa-times"></div>

    <form method="post">
      <h3>sign in</h3>
      <span>username</span>
      <input type="text" class="box" placeholder="enter your username" name="username" id="" />
      <span>password</span>
      <input type="password" class="box" placeholder="enter your password" name="password" id="" />
      <div class="checkbox">
        <input type="checkbox" id="remember-me" />
        <label for="remember-me"> remember me</label>
      </div>
      <input type="submit" name="submit" value="sign in" class="btn" />
      <p>forget password ? <a href="#">click here</a></p>
      <p>don't have an account ? <a href="create.php">create one</a></p>
    </form>
  </div>

  <!-- home section starts  -->

  <section class="home" id="home">
    <div class="row">
      <div class="swiper books-slider">
        <div class="swiper-wrapper">
          <a href="#" class="swiper-slide"><img src="image/custom-book/Bibi-Gill.png" alt="" /></a>
          <a href="#" class="swiper-slide"><img src="image/custom-book/Cantik-Itu-Luka.jpg" alt="" /></a>
          <a href="#" class="swiper-slide"><img src="image/custom-book/Cerita-cerita-Bahagia-Hampir-Seluruhnya.jpg" alt="" /></a>
          <a href="#" class="swiper-slide"><img src="image/custom-book/Heartbreak-Motel.jpg" alt="" /></a>
          <a href="#" class="swiper-slide"><img src="image/custom-book/Home-Sweet-Loan.jpg" alt="" /></a>
          <a href="#" class="swiper-slide"><img src="image/custom-book/Hujan.jpg" alt="" /></a>
        </div>
        <img src="image/stand.png" class="stand" alt="" />
      </div>
    </div>
  </section>

  <!-- home section ense  -->

  <!-- icons section starts  -->

  <section class="icons-container">
    <div class="icons">
      <i class="fas fa-shipping-fast"></i>
      <div class="content">
        <h3>free shipping</h3>
        <p>order over $100</p>
      </div>
    </div>

    <div class="icons">
      <i class="fas fa-lock"></i>
      <div class="content">
        <h3>secure payment</h3>
        <p>100 secure payment</p>
      </div>
    </div>

    <div class="icons">
      <i class="fas fa-redo-alt"></i>
      <div class="content">
        <h3>easy returns</h3>
        <p>10 days returns</p>
      </div>
    </div>

    <div class="icons">
      <i class="fas fa-headset"></i>
      <div class="content">
        <h3>24/7 support</h3>
        <p>call us anytime</p>
      </div>
    </div>
  </section>

  <!-- icons section ends -->

  <!-- featured section starts  -->

  <section class="featured" id="featured">
    <h1 class="heading"><span>featured books</span></h1>

    <div class="swiper featured-slider">
      <div class="swiper-wrapper">
        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Cantik-Itu-Luka.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Cantik itu Luka</h3>
            <div class="price">Rp. 175.000<span>Rp. 200.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Cerita-cerita-Bahagia-Hampir-Seluruhnya.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Bahagia Seluruhnya</h3>
            <div class="price">Rp. 88.000<span>Rp. 100.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Kita-Pergi-Hari-Ini.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Kita Pergi Hari Ini</h3>
            <div class="price">Rp. 88.000<span>Rp. 100.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/The-Star-and-I.jpg" alt="" />
          </div>
          <div class="content">
            <h3>The Star and I</h3>
            <div class="price">Rp. 99.000<span>Rp. 120.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Bibi-Gill.png" alt="" />
          </div>
          <div class="content">
            <h3>Bibi Gill</h3>
            <div class="price">Rp. 88.000<span>Rp. 100.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Layangan-Putus--1-.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Layangan Putus</h3>
            <div class="price">Rp. 75.000<span>Rp. 80.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Hujan.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Hujan</h3>
            <div class="price">Rp. 78.000<span>Rp. 80.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Home-Sweet-Loan.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Home Sweet Loan</h3>
            <div class="price">Rp. 95.000<span>Rp. 99.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Heartbreak-Motel.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Heartbreak Motel</h3>
            <div class="price">Rp. 99.000<span>Rp. 110.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>

        <div class="swiper-slide box">
          <div class="icons">
            <a href="#" class="fas fa-search"></a>
            <a href="#" class="fas fa-heart"></a>
            <a href="#" class="fas fa-eye"></a>
          </div>
          <div class="image">
            <img src="image/custom-book/Laut-Bercerita.jpg" alt="" />
          </div>
          <div class="content">
            <h3>Laut Bercerita</h3>
            <div class="price">Rp. 110.000 <span>Rp. 100.000</span></div>
            <a href="#" class="btn">add to cart</a>
          </div>
        </div>
      </div>

      <div class="swiper-button-next"></div>
      <div class="swiper-button-prev"></div>
    </div>
  </section>

  <!-- featured section ends -->

  <!-- deal section starts  -->

  <section class="deal">
    <div class="content">
      <h3>deal of the day</h3>
      <h1>upto 50% off</h1>
      <p>
        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Unde
        perspiciatis in atque dolore tempora quaerat at fuga dolorum natus
        velit.
      </p>
      <a href="#" class="btn">shop now</a>
    </div>

    <div class="image">
      <img src="image/deal-img.jpg" alt="" />
    </div>
  </section>

  <!-- deal section ends -->

  <!-- footer section starts  -->

  <section class="footer">
    <div class="box-container">
      <div class="box">
        <h3>contact info</h3>
        <a href="https://twitter.com/nugraha_______">
          <i class="fas fa-phone"></i> +123-456-7890
        </a>
        <a href="bimayuliajinugraha04@gmail.com">
          <i class="fas fa-envelope"></i> bimayuliajinugraha04@gmail.com
        </a>
      </div>
    </div>

    <div class="share">
      <a href="https://twitter.com/nugraha_______" class="fab fa-twitter"></a>
      <a href="https://instagram.com/nugraha_______" class="fab fa-instagram"></a>
      <a href="bimayuliajinugraha04@gmail.com" class="fas fa-envelope"></a>
    </div>

    <div class="credit">
      created by <span>amiByn</span> | all rights reserved!
    </div>
  </section>

  <!-- footer section ends -->

  <!-- loader  -->

  <!-- <div class="loader-container">
    <img src="image/loader-img.gif" alt="" />
  </div> -->

  <script src="https://unpkg.com/swiper@7/swiper-bundle.min.js"></script>

  <!-- custom js file link  -->
  <script src="js/script.js"></script>
</body>

</html>